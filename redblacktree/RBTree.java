



class RBTree<T extends Comparable<T>>
{
	final private  Node NULL =  new Node();
	private Node root=NULL;

	public enum Color {RED, BLACK};


	class Node
	{
		private T key;
		public Color color = Color.BLACK;
		private Node left = NULL, right = NULL;
		private Node p = NULL;
		public Node()
		{

		}
		public Node(T k)
		{
			key=k;
		}
		public boolean checkRoot()
		{
			return (p==null && root.color==Color.BLACK);
		}
		public boolean checkNodeRed()
		{
			if(color==Color.RED)
				return ((left==null|| left.color==Color.BLACK)&&(right==null|| right.color==color.BLACK));
			return true;
		}
		public int checkRBProperty()
		{
			if(left!= null && left.checkRBProperty()==-1)
				return -1;
			if(right!= null && right.checkRBProperty()==-1)
				return -1;
			if(left==null)
				if(right==null||right.checkRBProperty()==1)
					return 1;
				else
					return -1;
			if(right==null)
				if(left==null||left.checkRBProperty()==1)
					return 1;
				else
					return -1;
			return -1;	
		}	
	}

	public static void main(String[] args) 
	{

	}

	public RBTree(){}

	public  void printBinaryTree(Node root, int level)
	{
		if(root==null)
			return;
		printBinaryTree(root.right, level+1);
		if(level!=0)
		{
			for(int i=0; i<level-1; i++)
				System.out.print("|\t");
			System.out.print("|-------");
			if(root.color == Color.BLACK)
				System.out.println("(("+root.key+"))");
			else
				System.out.println("("+root.key+")");
		}
		else
		{
			if(root.color == Color.BLACK)
				System.out.println("(("+root.key+"))");
			else
				System.out.println("("+root.key+")");
		}
		printBinaryTree(root.left, level+1);
	} 
	public void print(RBTree tree)
	{
		if(tree.root == null)
			System.out.println("There is no RBTree");
		else
			printBinaryTree(tree.root, 0);
	}

	public  void insertElem(T n)
	{
		Node node = new Node(n);
		insert(node);

	}

	private void leftRotate(Node x) {
		Node y = x.right;
		x.right = y.left;
		if(y.left!=NULL)
		{
			y.left.p = x;
		}
		y.p = x.p;
		if(x.p==NULL)
		{
			root = y;
		}else if(x==x.p.left)
		{
			x.p.left = y;
		}else
		{
			x.p.right = y;
		}
		y.left = x;
		x.p = y;
	}

	private void rightRotate(Node x) {
		Node y = x.left;
		x.left = y.right;
		if(y.right!=NULL)
		{
			y.right.p = x;
		}
		y.p = x.p;
		if(x.p==NULL)
		{
			root = y;
		}else if(x==x.p.right)
		{
			x.p.right = y;
		}else
		{
			x.p.left = y;
		}
		y.right = x;
		x.p = y;
	}

	public boolean insert(Node z)
	{
		if(itsNode(root,z.key))
			return false;
		else
		{
			if(root==NULL)
				root = z;
			else
			{
				Node y = null;
				Node x = this.root;

				while(x!=NULL)
				{
					y = x;
					if(z.key.compareTo(x.key) == -1)
						x = x.left;
					else
						x = x.right;
				}
				z.p = y;
				if(y==NULL)
					this.root = z;
				else if(z.key.compareTo(y.key) == -1)
					y.left = z;
				else
					y.right = z;
				z.right = NULL;
				z.left = NULL;
				z.color = Color.RED;
			}
			insertFixup(z);
			return true;
		}

	}

	private void insertFixup(Node z)
	{
		Node y;
		if(z.p != NULL && z.p.p != NULL)
		{
			while(z.p.color == Color.RED){
				//case right
				if(z.p==z.p.p.left){
					y = z.p.p.right;
					if(y.color == Color.RED){
						z.p.color = Color.BLACK;
						y.color = Color.BLACK;
						z.p.p.color = Color.RED;
						z = z.p.p;
					}else{
						if(z==z.p.right){
							z = z.p;
							leftRotate(z);
						}
						z.p.color = Color.BLACK;
						z.p.p.color = Color.RED;
						rightRotate(z.p.p);
					}

					//case left
				}else{  
					y = z.p.p.left;
					if(y.color == Color.RED){
						z.p.color = Color.BLACK;
						y.color = Color.BLACK;
						z.p.p.color = Color.RED;
						z = z.p.p;
					}else{
						if(z==z.p.left){
							z = z.p;
							rightRotate(z);              
						}
						z.p.color = Color.BLACK;
						z.p.p.color = Color.RED;
						leftRotate(z.p.p);      
					}
				}
			}
			root.color = Color.BLACK;
		}
		else
		{
			z=root;
			root.color = Color.BLACK;
		}

	}

	private void transplant(Node n1, Node n2)
	{
		if(n1.p == null)
			root = n2;
		else if(  n2 == null && n1 == n1.p.left )
			n1.p.left = null;
		else if( n2 == null && n1 == n1.p.right )
			n1.p.right = null;
		else if( n1 == n1.p.left )
			n1.p.left = n2;
		else
			n1.p.right = n2;
		if(n2 != null)
			n2.p = n1.p;
	}

	private Node minimum(Node n)
	{
		if(n.left == null)
			return n;
		return minimum(n.left);
	}

	public boolean deleteElem(T n)
	{
		if(!itsNode(root,n))
		{
			return false;
		}
		else
		{
			Node node=foudNode(root,n);
			delete(node);
			return true;
		}
	}

	private Node foudNode (Node node,T n)
	{
		if(node!=NULL && node.key.compareTo(n)==0)
		{
			System.out.println("Entro al if"+node.key);
			return node;
		}
		else if(node.left!=NULL &&node.left.key.compareTo(n)==0)
		{
			return node.left;
		}
		else if(node.right!=NULL&&node.right.key.compareTo(n)==0)
		{
			return node.right;
		}
		else if(node.right!=NULL &&itsNode(node.right,n))
		{
			return foudNode(node.right,n);
		}
		else
			return foudNode(node.left,n);

	}

	private boolean itsNode(Node node,T elem) 
	{
		if (node==NULL)
			return false;
		else if (elem.compareTo(node.key)==0)
			return true;
		else
			return itsNode(node.left, elem) || itsNode(node.right, elem);
	}

	private void delete(Node z)
	{
		Node y = z;
		Color ycolor = y.color;
		Node x = NULL;//can change
		if(z.left == NULL && z.right == NULL)
		{
			if(z.p.left == z)
				z.p.left = NULL;
			else
				z.p.right = NULL;
		}
		else if(z.left == NULL)
		{
			x = z.right;
			transplant(z, z.right);
		}
		else if(z.right == NULL)
		{
			x = z.left;
			transplant(z, z.left);
		}
		else
		{
			System.out.println("entra la tercer");
			y = minimum(z.right);
			ycolor = y.color;
			x = y.right;
			System.out.println(y.key);
			if(y.p != z)
			{
				System.out.println("entra1");
				transplant(y, y.right);
				y.right = z.right;
				y.right.p = y;
			}
			transplant(z, y);
			y.left = z.left;
			y.left.p = y;
			y.color = z.color;
		}
		if(ycolor == Color.BLACK)
			deleteFixup(x);
	}
	private void deleteFixup(Node x)
	{
		while( x != NULL && x != root && x.color == Color.BLACK)
		{
			Node w;
			if( x == x.p.left )
			{
				w = x.p.right;
				if(w.color == Color.RED)
				{
					w.color = Color.BLACK;
					x.p.color = Color.RED;
					leftRotate(x.p);
					w = x.p.right;
				}
				if(w.left.color == Color.BLACK && w.right.color== Color.BLACK)
				{
					w.color = Color.RED;
					x = x.p;
				}
				else if(w.right.color == Color.BLACK)
				{
					w.left.color = Color.BLACK;
					w.color = Color.RED;
					rightRotate(w);
					w = x.p.right;
				}
				w.color = x.p.color;
				x.p.color = Color.BLACK;
				w.right.color = Color.BLACK;
				leftRotate(x.p);
				x = root;
			}
			else
			{
				w = x.p.left;
				if(w.color == Color.RED)
				{
					w.color = Color.BLACK;
					x.p.color = Color.RED;
					rightRotate(x.p);
					w = x.p.left;
				}
				if(w.left.color == Color.BLACK && w.right.color== Color.BLACK)
				{
					w.color = Color.RED;
					x = x.p;
				}
				else if(w.left.color == Color.BLACK)
				{
					w.right.color = Color.BLACK;
					w.color = Color.RED;
					rightRotate(w);
					w = x.p.left;
				}
				w.color = x.p.color;
				x.p.color = Color.BLACK;
				w.left.color = Color.BLACK;
				rightRotate(x.p);
				x = root;
			}

		}
		if(x != NULL)
			x.color = Color.BLACK;	
	}

	//Sanity check
	public boolean sanityCheck(RBTree tree)
	{
		if(tree.root.color == Color.RED)
			throw new RuntimeException("Error: color root"); 
		else if(checkProperty(tree.root) != 1)
			throw new RuntimeException("Error: every simple path has not the same number of black nodes");
		else
			System.out.println("OK");
		return true;
	}

	private int checkProperty(Node node) 
	{
		//Return 1 if checkProperty is OK
		if(node == null)
			return 1;
		else
		{
			if(node.color == Color.RED)
				if(node.left != null && node.left.color == Color.RED ||
				node.right != null && node.right.color == Color.RED)
					return 0;
			int s = 0;
			int izq; 
			int der;
			if(node.color == Color.BLACK)
				s = 1;

			izq = s + checkProperty(node.left);
			der = s + checkProperty(node.right);
			if(izq == der)
				return 1;
			return 0;
		}

	}

}


