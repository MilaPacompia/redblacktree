import static org.junit.Assert.*;

import org.junit.Test;



public class RBTreeTest {
	
	@Test
	public void testInsert1() {
		RBTree a = new RBTree();
		a.insertElem("a");
		a.insertElem("k");
		a.insertElem("z");
		a.insertElem("d");
		a.insertElem("m");
		a.insertElem("n");
		a.insertElem("c");
		a.insertElem("b");
		a.insertElem("e");
		assertEquals(true,a.sanityCheck(a));
		a.print(a);
	}
	@Test
	public void testDelete() {
		RBTree a = new RBTree();
		a.insertElem("a");
		a.insertElem("k");
		a.insertElem("z");
		a.insertElem("d");
		a.insertElem("m");
		a.insertElem("n");
		a.insertElem("c");
		a.insertElem("b");
		a.insertElem("e");
		a.print(a);
		a.deleteElem("a");
		assertEquals(true,a.sanityCheck(a));
		a.print(a);
		a.deleteElem("e");
		a.deleteElem("m");
		assertEquals(true,a.sanityCheck(a));
		a.print(a);
	}
	@Test
	public void testDelete2()
	{
		RBTree tree = new RBTree();
		tree.insert(tree.new Node(2));
		tree.insert(tree.new Node(6));
		tree.insert(tree.new Node(10));
		tree.insert(tree.new Node(14));
		tree.insert(tree.new Node(20));
		System.out.println("New");
		tree.print(tree);
		//Deleting
		tree.deleteElem(2);
		assertEquals(true,tree.sanityCheck(tree));
		tree.deleteElem(6);
		assertEquals(true,tree.sanityCheck(tree));
		tree.deleteElem(14);
		assertEquals(true,tree.sanityCheck(tree));
		tree.deleteElem(20);
		assertEquals(true,tree.sanityCheck(tree));
		tree.deleteElem(10);
		assertEquals(true,tree.sanityCheck(tree));

	}
	@Test
	public void testInsert2() {
		RBTree a = new RBTree();
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);
		a.insertElem(5);

		assertEquals(true,a.sanityCheck(a));
		a.print(a);
		a.deleteElem(5);
		assertEquals(true,a.sanityCheck(a));
	}
	@Test
	public void testInsert3() {
		RBTree a = new RBTree();
		a.insertElem(-1);
		a.insertElem(0);
		a.insertElem(1);
		a.insertElem(2);
		a.insertElem(53);
		a.insertElem(34);
		a.insertElem(42);
		a.insertElem(66);
		a.insertElem(51);
		a.insertElem(45);
		a.insertElem(42);
		a.insertElem(87);
		a.insertElem(15);
		a.insertElem(19);
		a.insertElem(59);
		a.insertElem(143);

		assertEquals(true,a.sanityCheck(a));
		a.print(a);
		a.deleteElem(-1);
		a.deleteElem(2);
		a.deleteElem(87);
		a.deleteElem(15);
		a.deleteElem(19);
		a.deleteElem(53);
		a.deleteElem(45);
		a.deleteElem(59);
		a.deleteElem(143);
		assertEquals(true,a.sanityCheck(a));
	}

	@Test
	public void testInsert4() {
		RBTree a = new RBTree();
		a.insertElem('a');
		a.insertElem('5');
		a.insertElem('7');
		a.insertElem('d');
		a.insertElem('m');
		a.insertElem('1');
		a.insertElem('c');
		a.insertElem('9');
		a.insertElem('p');
		a.insertElem('0');
		a.insertElem('6');
		a.insertElem('b');
		a.insertElem('4');
		assertEquals(true,a.sanityCheck(a));
		a.print(a);
		
	}
	
}
